# Description

Python package for reading Gizmo simulation snapshot files at z = 0.
Designed for use with the Latte suite of FIRE-2 simulations of Milky Way-mass galaxies, though usable with any Gizmo simulation snapshot files at z = 0.
Up-to-date Git (version control) repository of this package is available at: https://bitbucket.org/awetzel/gizmo_read

This is a light-weight version of the more feature-rich Gizmo reader/analysis package that I develop: https://bitbucket.org/awetzel/gizmo_analysis

author: Andrew Wetzel <arwetzel@gmail.com>


---
# Requirements

Required packages: python (2 or 3), numpy, scipy, h5py.

I developed this package using Python 3.7 and recommend that you use that version. However, I have tried to maintain backward compatibility with Python 2.7.


---
# Contents

## read.py
* read snapshot files at z = 0 from a Gizmo simulation

## center.py
* compute galaxy center coordinates, principal axes rotation vectors, and local standard of rest (solar) coordinates (relevant for MW-mass simulations)

## coordinate.py
* coordinate transformation, compute distances and velocities

## constant.py
* (astro)physical constants and unit conversions, including solar abundances

## gizmo_read_tutorial.ipynb
* jupyter notebook tutorial for using this package and reading particle data from a Gizmo snapshot at z = 0


---
# Units

Gizmo stores quantities in snapshot files using the following units by default:
* mass in [10^10 h^-1 M_sun]
* position, distance, radius in [h^-1 kpc comoving]
* velocity in [sqrt(scalefactor) * km/s]
* time in [scalefactor]
* elemental abundance in [(linear) mass fraction]

where scalefactor = 1/(1+z) is the expansion scale factor (at z = 0, scalefactor = 1).
Full documentation for units in Gizmo snapshots: http://www.tapir.caltech.edu/~phopkins/Site/GIZMO_files/gizmo_documentation.html#snaps-units

However, this python reader converts quantities to more convenient and physical units.
Unless otherwise noted, this reader converts all quantities to the following units (and combinations thereof):
* mass in [M_sun]
* position, distance, radius in [kpc physical]
* velocity in [km / s]
* time, age in [Gyr]
* elemental abundance in [(linear) mass fraction]
* metallicity in log10(mass_fraction / mass_fraction_solar), assuming Asplund et al 2009 for solar


---
# Usage

## Reading a snapshot

Within a simulation directory, read all particles in a snapshot at redshift 0 via:
```python
    part = read.Read.read_snapshot()
```
part is a dictionary, with a key for each particle species. So, access star particle dictionary via:
```python
    part['star']
```
part['star'] is dictionary, with each property as a key. For example:
```python
    part['star']['mass']
```
returns a numpy array of masses, one for each star particle, while
```python
    part['star']['position']
```
returns a numpy array of positions, of dimension particle_number x 3.


## Particle species

The available particle species in a cosmological simulation are:
```python
    part['dark'] : # dark matter at the highest resolution
    part['dark.2'] : # dark matter at lower resolution (outside of the zoom-in region)
    part['gas'] : # gas
    part['star'] : # stars
```


## Default/stored particle properties

Access these via:
```python
    part[species_name][property_name]
```

For example:
```python
    part['star']['position']
```


All particle species have the following properties:
```python
    'id' : # ID (indexing starts at 0)
    'position' : # 3-D position wrt galaxy center, aligned with galaxy principal axes [kpc physical]
    'velocity' : # 3-D velocity wrt galaxy center, aligned with galaxy principal axes [km / s]
    'mass' : # mass [M_sun]
    'potential' : # potential (computed using all particles in the simulation) [km^2 / s^2 physical]
```


Star and gas particles also have:
```python
    'massfraction' : # fraction of the mass that is in different elemental abundances, stored as an array for each particle, with indexes as follows:
                     # 0 = all metals (everything not H, He), 1 = He, 2 = C, 3 = N, 4 = O, 5 = Ne, 6 = Mg, 7 = Si, 8 = S, 9 = Ca, 10 = Fe
    # these also are stored as metallicity := log10(mass_fraction / mass_fraction_solar), where mass_fraction_solar is from Asplund et al 2009
    'metallicity.total' : everything not H, He
    'metallicity.he' : # Helium
    'metallicity.c' : # Carbon
    'metallicity.n' : # Nitrogen
    'metallicity.o' : # Oxygen
    'metallicity.ne' : # Neon
    'metallicity.mg' : # Magnesium
    'metallicity.si' : # Silicon
    'metallicity.s' : # Sulfur
    'metallicity.ca' : # Calcium
    'metallicity.fe' : # Iron
```

Star particles also have:
```python
    'form.scalefactor' : # expansion scale-factor when the star particle formed [0 to 1]
    'age' : # current age (t_now - t_form) [Gyr]
```

Gas particles also have:
```python
    'density' : # [M_sun / kpc^3]
    'temperature' : # [K]
    'electron.fraction' : # free-electron number per proton, averaged over mass of particle
    'hydrogen.neutral.fraction' : # fraction of hydrogen that is neutral (not ionized)
    'sfr' : # instantaneous star formation rate [M_sun / yr]
    'smooth.length' : # smoothing/kernel length, stored as Plummer-equivalent (for consistency with force softening) [kpc physical]
```


---
# Further documentation

[Gizmo users guide](http://www.tapir.caltech.edu/~phopkins/Site/GIZMO_files/gizmo_documentation.html) provides comprehensive documentation of the Gizmo code and contents of simulation snapshots.

[Gizmo source code (publicly available version)](https://bitbucket.org/phopkins/gizmo-public)

[Hopkins 2015](http://adsabs.harvard.edu/abs/2015MNRAS.450...53H) describes the Gizmo code and MFM hydrodynamics method.

[Hopkins et al 2018](https://ui.adsabs.harvard.edu/#abs/2017arXiv170206148H) describes the FIRE-2 physics model.

[Wetzel et al 2016](https://ui.adsabs.harvard.edu/#abs/2016ApJ...827L..23W) introduces the Latte suite of FIRE-2 simulations of Milky Way-mass galaxies.

[Sanderson et al 2018](https://arxiv.org/abs/1806.10564v1) describes the Ananke framework for generating synthetic Gaia-DR2 surveys from m12f, m12i, and m12m of the Latte suite and presents the properties of these simulations in detail.

[FIRE project website](https://fire.northwestern.edu)


---
# Citation

If you use any of the Latte FIRE-2 simulations, please including the following citation:

"The Latte suite of FIRE-2 cosmological zoom-in baryonic simulations of Milky Way-mass galaxies (Wetzel et al 2016), part of the Feedback In Realistic Environments (FIRE) simulation project, were run using the Gizmo gravity plus hydrodynamics code in meshless finite-mass (MFM) mode (Hopkins 2015) and the FIRE-2 physics model (Hopkins et al 2018)."


---
# License

Copyright 2018- by Andrew Wetzel <arwetzel@gmail.com>.

In summary, you are free to use, edit, share, and do whatever you want. But please report bugs and problems. Have fun!

Less succinctly, this software is governed by the MIT License:

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
